import Link from "next/link";

export default function ErrorPage() {
  return (
    <div className="bg-white">
      <main className="align-middle">
        <div className="max-w-7xl mx-auto justify-center items-center flex h-[calc(100vh-170px)] py-16 px-4 sm:px-6 lg:px-8">
          <div className="text-center">
            <p className="text-base font-semibold text-red-600">404</p>
            <h1 className="mt-4 text-3xl font-bold tracking-tight text-gray-900 sm:text-5xl">
              Page not found
            </h1>
            <p className="mt-6 text-base leading-7 text-gray-600">
              Sorry, we couldn’t find the page you’re looking for.
            </p>
            <div className="mt-10 flex items-center justify-center gap-x-6">
              <Link
                href="/"
                className="flex items-center justify-center bg-gradient-to-r from-rose-500 to-red-600 bg-origin-border px-4 py-2 border border-transparent rounded-md shadow-sm text-base font-medium text-white hover:from-rose-700 hover:to-red-800"
              >
                Go Back Home
              </Link>
            </div>
          </div>
        </div>
      </main>
    </div>
  );
}
