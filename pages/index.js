import Head from 'next/head'
import Landing from './landing/landingPage'

export default function Home() {
  return (
    <div>
      <Head>
        <title>Gamifikasi - Official Website</title>
        <meta name="description" content="lorem ipsum" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Landing />
   </div>
  )
}
