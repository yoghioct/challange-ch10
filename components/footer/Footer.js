import { Fragment } from "react";
import { Popover, Transition } from "@headlessui/react";
import { MenuIcon, XIcon } from "@heroicons/react/outline";
function classNames(...classes) {
  return classes.filter(Boolean).join(" ");
}

const Footer = () => {
  return (
    <footer className="bg-gray-50" aria-labelledby="footer-heading">
        <div className="max-w-7xl mx-auto pt-6 pb-6 px-4 lg:pt-6 lg:px-6 sm:px-6 pt-6">
          <p className="text-base text-gray-400 md:mt-0 md:order-1">
            &copy; 2023 Made by Team 2.
          </p>
        </div>
      </footer>
  )
}

export default Footer