import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Image from "next/image";

const SliderSponsor = () => {
  const settings = {
    dots: false,
    infinite: true,
    slidesToShow: 3,
    centerMode: true,
    slidesToScroll: 1,
    autoplay: true,
    speed: 3000,
    autoplaySpeed: 3000,
    cssEase: "linear",
    responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            infinite: true,
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            initialSlide: 2
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
  };

  return (
        <Slider {...settings}>
          <div>
            <Image
              className="h-12"
              src="/logo-binar-academy.svg"
              alt="Workcation"
              width={200}
              height={100}
            />
          </div>
          <div>
            <Image
              className="h-12"
              src="/logo-binar-academy.svg"
              alt="Workcation"
              width={200}
              height={100}
            />
          </div>
          <div>
            <Image
              className="h-12"
              src="/logo-binar-academy.svg"
              alt="Workcation"
              width={200}
              height={100}
            />
          </div>
          <div>
            <Image
              className="h-12"
              src="/logo-binar-academy.svg"
              alt="Workcation"
              width={200}
              height={100}
            />
          </div>
        </Slider>
  );
};

export default SliderSponsor;
