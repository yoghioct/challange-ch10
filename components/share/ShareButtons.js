import React from 'react'
import {
  FacebookShareButton,
  FacebookIcon,
  TwitterShareButton,
  TwitterIcon,
  WhatsappShareButton,
  WhatsappIcon,
} from 'next-share';
  
export default function ShareButtons() {
  return (
    <div className="shareBtn">
      <FacebookShareButton
        url={'http://localhost:3000'} >
        <FacebookIcon size={32} round />
      </FacebookShareButton>
      <WhatsappShareButton
        url={'http://localhost:3000'} >
        <WhatsappIcon size={32} round />
      </WhatsappShareButton>
      <TwitterShareButton
        url={'http://localhost:3000'} >
        <TwitterIcon size={32} round />
      </TwitterShareButton>
    </div>
  )
}