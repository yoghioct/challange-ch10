import { Fragment } from "react";
import { Popover, Transition } from "@headlessui/react";
import { MenuIcon, XIcon } from "@heroicons/react/outline";
import Link from "next/link";

function classNames(...classes) {
  return classes.filter(Boolean).join(" ");
}

const Navbar = () => {
  return (
    <header>
        <Popover className="relative bg-white">
          <div className="flex justify-between items-center max-w-7xl mx-auto px-4 py-6 sm:px-6 md:justify-start md:space-x-10 lg:px-8">
            <div className="flex justify-start lg:w-0 lg:flex-1">
              <Link href="/">
              <h2 className="text-4xl font-extrabold tracking-tight text-gray-900 sm:text-4xl">
              <span className="-mb-1 pb-1 block bg-gradient-to-r from-rose-500 to-red-600 bg-clip-text text-transparent">
                Team 2
              </span>
            </h2>
              </Link>
            </div>
            <div className="-mr-2 -my-2 md:hidden">
              <Popover.Button className="bg-white rounded-md p-2 inline-flex items-center justify-center text-gray-400 hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-indigo-500">
                <span className="sr-only">Open menu</span>
                <MenuIcon className="h-6 w-6" aria-hidden="true" />
              </Popover.Button>
            </div>
            <Popover.Group as="nav" className="hidden md:flex space-x-10">
              <Link
                href="/game"
                className="text-base font-medium text-gray-500 hover:text-gray-900"
              >
                Lihat Game
              </Link>
              <Link
                href="/dummygames"
                className="text-base font-medium text-gray-500 hover:text-gray-900"
              >
                Dummy Game
              </Link>
              <Link
                href="#"
                onClick={() => { alert('maintenance') }}
                className="text-base font-medium text-gray-500 hover:text-gray-900"
              >
                
                Tentang Kami
              </Link>
              <Link
                href="#"
                onClick={() => { alert('maintenance') }}
                className="text-base font-medium text-gray-500 hover:text-gray-900"
              >
                Blog
              </Link>
            </Popover.Group>
            <div className="hidden md:flex items-center justify-end md:flex-1 lg:w-0">
              <Link
                href="/login"
                onClick={() => {  }}
                className="whitespace-nowrap text-base font-medium text-gray-500 hover:text-gray-900"
              >
                Masuk
              </Link>
              <Link
                href="/register"
                onClick={() => { }}
                className="ml-8 whitespace-nowrap inline-flex items-center justify-center bg-gradient-to-r from-rose-500 to-red-600 bg-origin-border px-4 py-2 border border-transparent rounded-md shadow-sm text-base font-medium text-white hover:from-rose-700 hover:to-red-800"
              >
                Daftar
              </Link>
            </div>
          </div>

          <Transition
            as={Fragment}
            enter="duration-200 ease-out"
            enterFrom="opacity-0 scale-95"
            enterTo="opacity-100 scale-100"
            leave="duration-100 ease-in"
            leaveFrom="opacity-100 scale-100"
            leaveTo="opacity-0 scale-95"
          >
            <Popover.Panel
              focus
              className="absolute z-30 top-0 inset-x-0 p-2 transition transform origin-top-right md:hidden"
            >
              <div className="rounded-lg shadow-lg ring-1 ring-black ring-opacity-5 bg-white divide-y-2 divide-gray-50">
                <div className="pt-5 pb-6 px-5">
                  <div className="flex items-center justify-between">
                    <div>
                      <span className="-mb-1 pb-1 block bg-gradient-to-r from-rose-500 to-red-600 bg-clip-text text-transparent font-bold text-lg">
                        Menu
                      </span>
                    </div>
                    <div className="-mr-2">
                      <Popover.Button className="bg-white rounded-md p-2 inline-flex items-center justify-center text-gray-400 hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-indigo-500">
                        <span className="sr-only">Close menu</span>
                        <XIcon className="h-6 w-6" aria-hidden="true" />
                      </Popover.Button>
                    </div>
                  </div>
                  <div className="mt-6">
                    <nav className="grid grid-cols-1 gap-7">
                      <Link
                        key="1"
                        href="/game"
                        className="-m-3 p-3 flex items-center rounded-lg hover:bg-gray-50"
                      >
                        <div className="ml-4 text-base font-medium text-gray-900">
                          Lihat Game
                        </div>
                      </Link>
                      <Link
                        key="2"
                        href="#"
                        onClick={() => { alert('maintenance') }}
                        className="-m-3 p-3 flex items-center rounded-lg hover:bg-gray-50"
                      >
                        <div className="ml-4 text-base font-medium text-gray-900">
                          Tentang Kami
                        </div>
                      </Link>
                      <Link
                        key="3"
                        href="#"
                        onClick={() => { alert('maintenance') }}
                        className="-m-3 p-3 flex items-center rounded-lg hover:bg-gray-50"
                      >
                        <div className="ml-4 text-base font-medium text-gray-900">
                          Blog
                        </div>
                      </Link>
                    </nav>
                  </div>
                </div>
                <div className="py-6 px-5">
                  <div className="mt-6">
                    <Link
                      href="#"
                      className="w-full flex items-center justify-center bg-gradient-to-r from-rose-500 to-red-600 bg-origin-border px-4 py-2 border border-transparent rounded-md shadow-sm text-base font-medium text-white hover:from-rose-700 hover:to-red-800"
                    >
                      Daftar
                    </Link>
                    <p className="mt-6 text-center text-base font-medium text-gray-500">
                      Sudah Punya Akun?&nbsp;
                      <Link href="#" className="text-gray-900">
                        Masuk
                      </Link>
                    </p>
                  </div>
                </div>
              </div>
            </Popover.Panel>
          </Transition>
        </Popover>
      </header>
  )
}

export default Navbar